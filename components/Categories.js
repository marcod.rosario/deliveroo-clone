import { View, Text, ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react'
import CategoryCard from './CategoryCard'
import sanityClient from "../sanity/sanity";

const Categories = () => {

  const [categories, setCategories] = useState([]);

  useEffect(() => {
    var query = `
      *[_type == "category"] 
      `;

    sanityClient
    .fetch(query)
    .then((data) => {
      setCategories(data)
    });
    
  }, []);


  return (
    <ScrollView 
    horizontal
    showsHorizontalScrollIndicator={false}
    contentContainerStyle= {{
      paddingHorizontal: 15,
      paddingTop: 10
    }}>

      {/* Category Card */}
      {categories?.map((cat) => (
          <CategoryCard
            key={cat._id}
            imgUrl={cat.image}
            title={cat.name}
          />
        ))}

    </ScrollView>
  )
}

export default Categories
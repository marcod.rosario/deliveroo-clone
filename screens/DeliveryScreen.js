import { View, Text, Image, StyleSheet } from "react-native";
import React from "react";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";
import { selectRestaurant } from "../features/restaurantSlice";
import { SafeAreaView } from "react-native-safe-area-context";
import { TouchableOpacity } from "react-native-web";
import { XMarkIcon } from "react-native-heroicons/outline";
import * as Progress from "react-native-progress";
// import MapView, { Marker } from "react-native-maps";

const DeliveryScreen = () => {
  const navigation = useNavigation();
  const restaurant = useSelector(selectRestaurant);
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    map: {
      width: "100%",
      height: "100%",
    },
  });

  return (
    <View className="bg-[#00CCBB] flex-1">
      <SafeAreaView className="z-50">
        <View className="flex-row justify-between items-center p-5">
          <Text className="text-lg font-light text-white">Order Help</Text>
          <TouchableOpacity
            // onPress={navigation.navigate("Home")}
            className="rounded-full absolute top-5 right-5"
          >
            <XMarkIcon color="white" height={25} width={25} />
          </TouchableOpacity>
        </View>

        <View className="bg-white mx-5 my-2 rounded-md p-6 z-50 shadow-md">
          <View className="flex-row justify-between">
            <View>
              <Text className="text-lg text-gray-400">Estimated Arrival</Text>
              <Text className="font-bold text-3xl">45-55 Minutes</Text>
            </View>
            <Image
              source={{
                uri: "https://links.papareact.com/fls",
              }}
              className="h-20 w-20"
            />
          </View>

          <Progress.Bar size={30} color="#00CCBB" indeterminate={true} />

          <Text className="mt-3 text-gray-500">
            Your order at {restaurant?.title} is being prepared
          </Text>
        </View>
      </SafeAreaView>
      {/* <View style={styles.container}>
        <MapView
            style={styles.map}
            initialRegion={{
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          />
          
      </View> */}
      <SafeAreaView className="z-50">
        <View className="bg-white mx-5 my-2 rounded-md p-6 z-50 shadow-md">
          <View className="flex-row justify-between">
            <Image
              source={{
                uri: "https://links.papareact.com/wru",
              }}
              className="w-10 h-10 bg-gray-300 p-4 rounded-full"
            />

            <View className="flex-1 mx-2">
              <Text>
                <Text className="font-bold text-l">Marco Rosário</Text>
              </Text>
              <Text className="font-bold text-gray-400 text-xs">Your Rider</Text>
            </View>

            <TouchableOpacity>
              <Text className="text-[#00CCBB] mt-2 font-bold">Call</Text>
            </TouchableOpacity>
          </View>

        </View>
      </SafeAreaView>
    </View>
  );
};

export default DeliveryScreen;
